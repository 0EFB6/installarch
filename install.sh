#!/bin/bash
set -a
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
SCRIPTS_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"/scripts
CONFIGS_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"/configs
set +a

echo -ne "
###################################################################
###################################################################
##   Highly Configurable Automated Vannila Arch Linux Installer  ##
###################################################################
##                        Made By Willy                          ##
###################################################################
###################################################################"
( bash $SCRIPTS_DIR/startup.sh )|& tee startup.log
source $CONFIGS_DIR/setup.conf
( bash $SCRIPTS_DIR/0-preinstall.sh )|& tee 0-preinstall.log
( arch-chroot /mnt $HOME/installarch/scripts/1-setup.sh )|& tee 1-setup.log
if [[ ! $DESKTOP_ENV == minimal ]]; then
  ( arch-chroot /mnt /usr/bin/runuser -u $USERNAME -- /home/$USERNAME/installarch/scripts/2-user.sh )|& tee 2-user.log
fi
( arch-chroot /mnt $HOME/installarch/scripts/3-post-setup.sh )|& tee 3-post-setup.log
cp -v *.log /mnt/home/$USERNAME
echo -ne "
###################################################################
###################################################################
##                  Finished Install Arch Linux                  ##
###################################################################
##              Please Eject Install Media and Reboot            ##
###################################################################
###################################################################"