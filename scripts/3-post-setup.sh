#!/usr/bin/env bash

source ${HOME}/installarch/configs/setup.conf
PARTID=$(blkid -s PARTUUID -o value ${partition3})

if [[ "${FS}" == "luks" ]]; then
cat <<EOF > /boot/loader/entries/arch.conf
title Arch Linux
linux /vmlinuz-linux
initrd /initramfs-linux.img
options cryptdevice=PARTUUID=$ENCRYPTED_PARTITION_UUID:cryptroot root=/dev/mapper/cryptroot rw 
EOF
fi

echo "###################################################################"
echo "##                Enabling Login Display Manager                 ##"
echo "###################################################################"
if [[ "${DESKTOP_ENV}" == "gnome" ]]; then
  systemctl enable gdm.service
elif [[ "${DESKTOP_ENV}" == "openbox" ]]; then
  systemctl enable lightdm.service
  if [[ "${INSTALL_TYPE}" == "FULL" ]]; then
    sed -i 's/^webkit_theme\s*=\s*\(.*\)/webkit_theme = litarvan #\1/g' /etc/lightdm/lightdm-webkit2-greeter.conf
    sed -i 's/#greeter-session=example.*/greeter-session=lightdm-webkit2-greeter/g' /etc/lightdm/lightdm.conf
  fi
else
  if [[ ! "${DESKTOP_ENV}" == "server"  ]]; then
  echo "Command line login is set."
  fi
fi

echo "###################################################################"
echo "##                  Enabling Essential Services                  ##"
echo "###################################################################"
ntpd -qg
systemctl enable ntpd.service
systemctl disable dhcpcd.service
systemctl enable NetworkManager.service
systemctl enable avahi-daemon.service
systemctl enable libvirtd
usermod -G libvirt -a ${USERNAME}

echo "###################################################################"
echo "##                        Cleaning up...                         ##"
echo "###################################################################"
sed -i 's/^%wheel ALL=(ALL) NOPASSWD: ALL/# %wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
sed -i 's/^%wheel ALL=(ALL:ALL) NOPASSWD: ALL/# %wheel ALL=(ALL:ALL) NOPASSWD: ALL/' /etc/sudoers
sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers
sed -i 's/^# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/' /etc/sudoers
rm -r $HOME/installarch
cd $pwd