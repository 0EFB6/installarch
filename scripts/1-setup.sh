#!/usr/bin/env bash

source $HOME/installarch/configs/setup.conf
# echo -ne '[willy_repo]\nSigLevel = Optional DatabaseOptional\nServer = https://gitlab.com/0EFB6/$repo/-/raw/main/$arch\n' >> /etc/pacman.conf
# pacman -Sy --noconfirm --needed
pacman -Sy --noconfirm --needed networkmanager pacman-contrib curl reflector rsync arch-install-scripts git sudo ntp dhcpcd dhcp
systemctl enable --now NetworkManager

echo "###################################################################"
echo "##                    Configuring CPU Core Counts                ##"
echo "###################################################################"
nc=$(grep -c ^processor /proc/cpuinfo)
sed -i "s/#MAKEFLAGS=\"-j2\"/MAKEFLAGS=\"-j$nc\"/g" /etc/makepkg.conf
sed -i "s/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T $nc -z -)/g" /etc/makepkg.conf

echo "###################################################################"
echo "##                    Setup Language and Locale                  ##"
echo "###################################################################"
sed -i 's/^#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
locale-gen
hwclock --systohc
ln -sf /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
timedatectl --no-ask-password set-timezone ${TIMEZONE}
timedatectl --no-ask-password set-ntp 1
localectl --no-ask-password set-locale LANG="en_US.UTF-8" LC_TIME="en_US.UTF-8"
localectl --no-ask-password set-keymap ${KEYMAP}

echo "###################################################################"
echo "##                    Configuring User Permission                ##"
echo "###################################################################"
sed -i 's/^# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
sed -i 's/^# %wheel ALL=(ALL:ALL) NOPASSWD: ALL/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/' /etc/sudoers
sed -i 's/#ParallelDownloads = 5/ParallelDownloads = 10/' /etc/pacman.conf
sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf
pacman -Sy --noconfirm --needed

echo "###################################################################"
echo "##                    Installing Base System                     ##"
echo "###################################################################"
if [[ $DESKTOP_ENV == "hyprland" ]]; then
  PACMAN_PKGS_FILE=pacman-pkgs-hyprland.txt
else
  PACMAN_PKGS_FILE=pacman-pkgs.txt
fi

if [[ ! $DESKTOP_ENV == server ]]; then
  sed -n '/'$INSTALL_TYPE'/q;p' ~/installarch/pkg-files/$PACMAN_PKGS_FILE | while read line
  do
    if [[ ${line} == '--END OF MINIMAL INSTALL--' ]]; then continue; fi
    echo "INSTALLING: ${line}"
    sudo pacman -S --noconfirm --needed ${line}
  done
fi

echo "###################################################################"
echo "##                      Installing Microcode                     ##"
echo "###################################################################"
proc_type=$(lscpu)
if grep -E "GenuineIntel" <<< ${proc_type}; then
    echo "Installing Intel microcode"
    pacman -S --noconfirm --needed intel-ucode
    proc_ucode=intel-ucode.img
elif grep -E "AuthenticAMD" <<< ${proc_type}; then
    echo "Installing AMD microcode"
    pacman -S --noconfirm --needed amd-ucode
    proc_ucode=amd-ucode.img
fi

echo "###################################################################"
echo "##                  Installing Graphics Drivers                  ##"
echo "###################################################################"
gpu_type=$(lspci)
if grep -E "NVIDIA|GeForce" <<< ${gpu_type}; then
    pacman -S --noconfirm --needed nvidia
	nvidia-xconfig
elif lspci | grep 'VGA' | grep -E "Radeon|AMD"; then
    pacman -S --noconfirm --needed xf86-video-amdgpu
elif grep -E "Integrated Graphics Controller" <<< ${gpu_type}; then
    pacman -S --noconfirm --needed libva-intel-driver libvdpau-va-gl lib32-vulkan-intel vulkan-intel libva-intel-driver libva-utils lib32-mesa
elif grep -E "Intel Corporation UHD" <<< ${gpu_type}; then
    pacman -S --needed --noconfirm libva-intel-driver libvdpau-va-gl lib32-vulkan-intel vulkan-intel libva-intel-driver libva-utils lib32-mesa
fi

echo "###################################################################"
echo "##        Please reenter your configurations (if prompt to)      ##"
echo "###################################################################"
if ! source $HOME/installarch/configs/setup.conf; then
	while true; do 
		read -p "Please enter username:" username
		if [[ "${username,,}" =~ ^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\$)$ ]]; then break; fi 
		echo "Incorrect username."
	done 
    echo "username=${username,,}" >> ${HOME}/installarch/configs/setup.conf
    read -p "Please enter password:" password
    echo "password=${password,,}" >> ${HOME}/installarch/configs/setup.conf
    while true; do 
		read -p "Please name your machine:" name_of_machine
		if [[ "${name_of_machine,,}" =~ ^[a-z][a-z0-9_.-]{0,62}[a-z0-9]$ ]]; then break; fi
		read -p "Hostname doesn't seem correct. Do you still want to save it? (y/n)" force 
		if [[ "${force,,}" = "y" ]]; then break; fi 
	done 
    echo "NAME_OF_MACHINE=${name_of_machine,,}" >> ${HOME}/installarch/configs/setup.conf
fi

echo "###################################################################"
echo "##                           Adding User                         ##"
echo "###################################################################"
if [ $(whoami) = "root"  ]; then
    groupadd libvirt
    useradd -m -G wheel,libvirt,audio,storage,video,optical -s /bin/bash $USERNAME 
    echo "User $USERNAME created, home directory created, default shell set to /bin/bash, password is also set"
    echo "$USERNAME:$PASSWORD" | chpasswd
	cp -R $HOME/installarch /home/$USERNAME/
    chown -R $USERNAME: /home/$USERNAME/installarch
    echo "installarch copied to home directory"
	echo $NAME_OF_MACHINE > /etc/hostname
	echo "127.0.0.1 localhost" >> /etc/hosts
	echo "::1       localhost" >> /etc/hosts
	echo "127.0.1.1 ${USERNAME}.localdomain ${USERNAME}" >> /etc/hosts
else
	echo "You are already a user proceed with aur installs"
fi

if [[ ${FS} == "luks" ]]; then
    sed -i 's/filesystems/encrypt filesystems/g' /etc/mkinitcpio.conf
    sed -i 's/modconf/keyboard modconf/g' /etc/mkinitcpio.conf
    sed -i 's/filesystems keyboard/filesystems/g' /etc/mkinitcpio.conf
    mkinitcpio -p linux
fi