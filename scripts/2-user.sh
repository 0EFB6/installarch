source $HOME/installarch/configs/setup.conf

echo "###################################################################"
echo "##                    Installing AUR Software                    ##"
echo "###################################################################"
if [[ $DESKTOP_ENV == "hyprland" ]]; then
  AUR_PKG_FILE=aur-pkgs-hyprland.txt
else
  AUR_PKG_FILE=aur-pkgs.txt
fi

if [[ ! $AUR_HELPER == none ]]; then
  cd ~ && git clone "https://aur.archlinux.org/$AUR_HELPER.git"
  cd ~/$AUR_HELPER &&  makepkg -si --noconfirm
  cd ~  && rm -rf $AUR_HELPER
  sed -n '/'$INSTALL_TYPE'/q;p' ~/installarch/pkg-files/$AUR_PKG_FILE | while read line
  do
    if [[ ${line} == '--END OF MINIMAL INSTALL--' ]]; then continue; fi
    echo "INSTALLING: ${line}"
    $AUR_HELPER -S --noconfirm --needed ${line}
  done
fi

echo "###################################################################"
echo "##                  Installing Virtual Machine                   ##"
echo "###################################################################"
sudo pacman -S --noconfirm --needed bridge-utils \
  dnsmasq \
  dnsutils \
  edk2-ovmf \
  inetutils \
  ipset \
  iptables \
  qemu \
  qemu-guest-agent \
  vde2 \
  virt-manager

echo "###################################################################"
echo "##               Installing Fonts & Chinese Input                ##"
echo "###################################################################"
sudo pacman -S --noconfirm --needed adobe-source-han-sans-cn-fonts \
  adobe-source-han-serif-cn-fonts \
  adobe-source-sans-pro-fonts \
  fcitx \
  fcitx-configtool \
  fcitx-sunpinyin \
  fcitx-googlepinyin \
  fcitx-qt5 \
  fontconfig \
  noto-fonts-cjk \
  noto-fonts \
  noto-fonts-emoji \
  opendesktop-fonts \
  terminus-font \
  ttf-anonymous-pro \
  ttf-arphic-ukai \
  ttf-arphic-uming \
  ttf-bitstream-vera \
  ttf-caladea \
  ttf-carlito \
  ttf-dejavu \
  ttf-droid \
  ttf-font-awesome \
  ttf-hack \
  ttf-joypixels \
  ttf-jetbrains-mono \
  ttf-jetbrains-mono-nerd \
  ttf-liberation \
  ttf-roboto \
  ttf-roboto-mono \
  ttf-space-mono-nerd \
  ttf-ubuntu-font-family

echo "###################################################################"
echo "##                 Installing Willy's Programs                   ##"
echo "###################################################################"
sudo pacman -S --noconfirm --needed baobab \
  bpytop \
  btop \
  chromium \
  code \
  discord \
  eog \
  feh \
  firefox \
  gnome-disk-utility \
  gprename \
  htop \
  imwheel \
  keepassxc \
  lynx \
  lm_sensors \
  mlocate \
  mousepad \
  mupdf \
  nmap \
  network-manager-applet \
  neofetch \
  nload \
  nwg-look \
  obs-studio \
  mpv \
  qalculate-gtk \
  rofi \
  scrcpy \
  speedtest-cli \
  spotify-launcher \
  termdown \
  wofi \
  webkit2gtk \
  veracrypt
sudo pacman -S --no-confirm --needed python-pywal

export PATH=$PATH:~/.local/bin
echo "###################################################################"
echo "##                  Installing DE/WM Software                    ##"
echo "###################################################################"
cd ~ && sed -n '/'$INSTALL_TYPE'/q;p' ~/installarch/pkg-files/${DESKTOP_ENV}.txt | while read line
do
  if [[ ${line} == '--END OF MINIMAL INSTALL--' ]]; then continue; fi
  echo "INSTALLING: ${line}"
  sudo pacman -S --noconfirm --needed ${line}
done

if [[ $DESKTOP_ENV == "dwm" ]]; then
  cd ~
  echo "feh --bg-scale .wallpapers/5.png &
  xrdb -merge ~/.Xresources &
  fcitx-autostart &
  export GTK_IM_MODULE=fcitx
  export QT_IM_MODULE=fcitx
  export XMODIFIERS=@im=fcitx
  exec dwm" > /home/$USERNAME/.xinitrc
  echo "Xcursor.theme: Adwaita
  Xcursor.size: 52" > /home/$USERNAME/.Xresources
  echo "startx" >> /home/$USERNAME/.bash_profile
  #echo "[[ $(fgconsole 2>/dev/null == 1 ]] && exec startx --vt1" >> $HOME/.bashrc
  git clone https://gitlab.com/0efb6/sh_config.git
  mkdir /home/$USERNAME/.config/
  cd /home/$USERNAME/.config/
  cp -r /home/$USERNAME/sh_config/config/* .
  cd /home/$USERNAME
  rm -rf .vim/
  git clone https://gitlab.com/0efb6/vim
  mv -f vim/ .vim/
fi

if [[ $DESKTOP_ENV == "hyprland" ]]; then
  echo "#!/bin/sh
  export WLR_NO_HARDWARE_CURSORS=1
  export WLR_RENDERER_ALLOW_SOFTWARE=1
  exec Hyprland" > /home/$USERNAME/start.sh
  git clone https://gitlab.com/0efb6/fonts-willy.git
  rm -rf fonts-willy/.git 
  rm fonts-willy/README.md
  sudo cp -r fonts-willy/* /usr/share/fonts/
  git clone https://gitlab.com/0efb6/themes-willy.git
  sudo cp -r themes-willy/Colloid-Dark-Nord themes-willy/Colloid-Dark-Dracula themes-willy/Colloid-Dark themes-willy/Arc-Dark themes-willy/Nordic-darker-standard-buttons themes-willy/Dracula themes-willy/Nordic-darker themes-willy/Nordic-darker-v40 themes-willy/Orchis-Green-Dark themes-willy/Orchis-Green-Dark-Compact /usr/share/themes
  git clone https://gitlab.com/0efb6/icons-willy.git
  sudo cp -r icons-willy/Win11-black icons-willy/Win11-black-dark icons-willy/Win11-green icons-willy/Win11-green-dark icons-willy/Dracula /usr/share/icons/
  git clone https://gitlab.com/willy45/wallpapers-willy.git
  rm -rf wallpapers-willy/.git
  rm wallpapers-willy/README.md
  mv wallpapers-willy .wallpaper
  git clone https://gitlab.com/0efb6/sh_config.git
  mkdir /home/$USERNAME/.config/
  cd /home/$USERNAME/.config/
  cp -r /home/$USERNAME/sh_config/config/* .
  cd /home/$USERNAME
  rm -rf .vim/
  git clone https://gitlab.com/0efb6/vim
  mv -f vim/ .vim/
  sudo pywalfox install
  wal -i /home/$USERNAME/.wallpaper/bg7.jpg
fi

if [[ $INSTALL_TYPE == "FULL" ]]; then
  if [[ $DESKTOP_ENV == "openbox" ]]; then
    cd ~
    git clone https://github.com/stojshic/dotfiles-openbox
    ./dotfiles-openbox/install-titus.sh
  fi
fi


echo -ne "
-------------------------------------------------------------------------
                    SYSTEM READY FOR 3-post-setup.sh
-------------------------------------------------------------------------"
exit