#!/usr/bin/env bash

source $CONFIGS_DIR/setup.conf
timedatectl set-ntp true
pacman -S --noconfirm archlinux-keyring pacman-contrib terminus-font
setfont ter-v22b
sed -i 's/#ParallelDownloads = 5/ParallelDownloads = 10/' /etc/pacman.conf
pacman -S --noconfirm --needed reflector rsync
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
reflector -a 48 -f 15 -l 20 --sort rate --save /etc/pacman.d/mirrorlist
mkdir /mnt &>/dev/null

echo "###################################################################"
echo "##                   Installing Prerequisities                   ##"
echo "###################################################################"
pacman -S --noconfirm --needed gptfdisk glibc

echo "###################################################################"
echo "##                        Formating Disk                         ##"
echo "###################################################################"
umount -A --recursive /mnt
partprobe ${DISK}

if [[ "${CUSTOMPART}" == "yes" ]]; then
    partition2=${DISK}${CUSTOMDISKBOOT}
    partition3=${DISK}${CUSTOMDISKROOT}
elif [[ "${DISK}" =~ "nvme" ]]; then
    sgdisk -Z ${DISK} # zap all on disk
    sgdisk -a 2048 -o ${DISK} # new gpt disk 2048 alignment
    sgdisk -n 1::+512M --typecode=1:ef00 --change-name=1:'EFIBOOT' ${DISK} # partition 1 (UEFI Boot Partition)
    sgdisk -n 2::-0 --typecode=2:8300 --change-name=2:'ROOT' ${DISK} # partition 2 (Root), default start, remaining
    partition2=${DISK}p1
    partition3=${DISK}p2
else
    sgdisk -Z ${DISK} # zap all on disk
    sgdisk -a 2048 -o ${DISK} # new gpt disk 2048 alignment
    sgdisk -n 1::+512M --typecode=1:ef00 --change-name=1:'EFIBOOT' ${DISK} # partition 1 (UEFI Boot Partition)
    sgdisk -n 2::-0 --typecode=2:8300 --change-name=2:'ROOT' ${DISK} # partition 2 (Root), default start, remaining
    partprobe ${DISK}
    partition2=${DISK}1
    partition3=${DISK}2
fi

if [[ "$INSTALL_BOOTLOADER" == "yes" ]]; then
    mkfs.vfat -F32 -n "EFIBOOT" ${partition2}
fi

if [[ "${FS}" == "ext4" ]]; then
    mkfs.ext4 -L ROOT ${partition3}
    mount -t ext4 ${partition3} /mnt
elif [[ "${FS}" == "luks" ]]; then
    echo -n "${LUKS_PASSWORD}" | cryptsetup -y -v luksFormat ${partition3} -
    echo -n "${LUKS_PASSWORD}" | cryptsetup open ${partition3} cryptroot -
    mkfs.ext4 -L ROOT /dev/mapper/cryptroot
    mount -t ext4 /dev/mapper/cryptroot /mnt
    echo ENCRYPTED_PARTITION_UUID=$(blkid -s PARTUUID -o value ${partition3}) >> $CONFIGS_DIR/setup.conf
fi

mkdir -p /mnt/boot
mount ${partition2} /mnt/boot/
echo "ERROR Encountered. Stopping installation scripts..."
sleep 3

if ! grep -qs '/mnt' /proc/mounts; then
    echo "Drive is not mounted can not continue"
    echo "Rebooting in 10 Seconds ..." && sleep 5 && echo "Rebooting in 5 Seconds ..." && sleep 2 && echo "Rebooting in 3 Second ..." && sleep 3
    reboot now
fi

echo "###################################################################"
echo "##                    Arch Install On Main Drive                 ##"
echo "###################################################################"
pacstrap /mnt base base-devel linux linux-firmware git vim nano sudo archlinux-keyring wget libnewt --noconfirm --needed
cp -R ${SCRIPT_DIR} /mnt/root/installarch
cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist
genfstab -L /mnt >> /mnt/etc/fstab
echo "Generated /etc/fstab"

echo "###################################################################"
echo "##               Installing Systemd-boot Bootloader              ##"
echo "###################################################################"
if [[ ! -d "/sys/firmware/efi" ]]; then
    echo "Installation ERROR! Only UEFI system is allowed!"
else
    if [[ "$INSTALL_BOOTLOADER" == "yes" ]]; then
        pacstrap /mnt efibootmgr --noconfirm --needed
        bootctl install --esp-path=/mnt/boot
        PARTID=$(blkid -s PARTUUID -o value ${partition3})
        mkdir /mnt/boot/arch
        cp /mnt/boot/initramfs-linux.img /mnt/boot/initramfs-linux-fallback.img /mnt/boot/vmlinuz-linux /mnt/boot/arch/
        cat <<EOF > /mnt/boot/loader/loader.conf
default arch
timeout 3
EOF
        cat <<EOF > /mnt/boot/loader/entries/arch.conf
title Arch Linux (Hyprland)
linux /arch/vmlinuz-linux
initrd /arch/initramfs-linux.img
options root=PARTUUID=$PARTID rw
EOF
    else
        PARTID=$(blkid -s PARTUUID -o value ${partition3})
        mkdir /mnt/boot/arch-$USERNAME
        cp /mnt/boot/initramfs-linux.img /mnt/boot/initramfs-linux-fallback.img /mnt/boot/vmlinuz-linux /mnt/boot/arch-$USERNAME/
        cat <<EOF > /mnt/boot/loader/loader.conf
default arch-$USERNAME
timeout 3
EOF
        cat <<EOF > /mnt/boot/loader/entries/arch-$USERNAME.conf
title WOS
linux /arch-$USERNAME/vmlinuz-linux
initrd /arch-$USERNAME/initramfs-linux.img
options root=PARTUUID=$PARTID rw
EOF
    fi
fi

echo -ne "
-------------------------------------------------------------------------
                    SYSTEM READY FOR 1-setup.sh
-------------------------------------------------------------------------"