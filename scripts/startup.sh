#!/usr/bin/env bash

CONFIG_FILE=$CONFIGS_DIR/setup.conf
if [ ! -f $CONFIG_FILE ]; then touch -f $CONFIG_FILE; fi

logo () {
  echo -ne "
  ########################################################################
                              Install Willy OS
  ########################################################################
              Please select presetup settings for your system              
  ########################################################################
  "
}

set_option() {
  if grep -Eq "^${1}.*" $CONFIG_FILE;then sed -i -e "/^${1}.*/d" $CONFIG_FILE; fi
  echo "${1}=${2}" >>$CONFIG_FILE
}

select_option() {
    ESC=$( printf "\033")
    cursor_blink_on()  { printf "$ESC[?25h"; }
    cursor_blink_off() { printf "$ESC[?25l"; }
    cursor_to()        { printf "$ESC[$1;${2:-1}H"; }
    print_option()     { printf "$2   $1 "; }
    print_selected()   { printf "$2  $ESC[7m $1 $ESC[27m"; }
    get_cursor_row()   { IFS=';' read -sdR -p $'\E[6n' ROW COL; echo ${ROW#*[}; }
    get_cursor_col()   { IFS=';' read -sdR -p $'\E[6n' ROW COL; echo ${COL#*[}; }
    key_input()         {
                        local key
                        IFS= read -rsn1 key 2>/dev/null >&2
                        if [[ $key = ""      ]]; then echo enter; fi;
                        if [[ $key = $'\x20' ]]; then echo space; fi;
                        if [[ $key = "k" ]]; then echo up; fi;
                        if [[ $key = "j" ]]; then echo down; fi;
                        if [[ $key = "h" ]]; then echo left; fi;
                        if [[ $key = "l" ]]; then echo right; fi;
                        if [[ $key = "a" ]]; then echo all; fi;
                        if [[ $key = "n" ]]; then echo none; fi;
                        if [[ $key = $'\x1b' ]]; then
                            read -rsn2 key
                            if [[ $key = [A || $key = k ]]; then echo up;    fi;
                            if [[ $key = [B || $key = j ]]; then echo down;  fi;
                            if [[ $key = [C || $key = l ]]; then echo right;  fi;
                            if [[ $key = [D || $key = h ]]; then echo left;  fi;
                        fi
    }
    print_options_multicol() {
        local curr_col=$1
        local curr_row=$2
        local curr_idx=0
        local idx=0
        local row=0
        local col=0
        curr_idx=$(( $curr_col + $curr_row * $colmax ))
        for option in "${options[@]}"; do
            row=$(( $idx/$colmax ))
            col=$(( $idx - $row * $colmax ))
            cursor_to $(( $startrow + $row + 1)) $(( $offset * $col + 1))
            if [ $idx -eq $curr_idx ]; then
                print_selected "$option"
            else
                print_option "$option"
            fi
            ((idx++))
        done
    }
    for opt; do printf "\n"; done
    local return_value=$1
    local lastrow=`get_cursor_row`
    local lastcol=`get_cursor_col`
    local startrow=$(($lastrow - $#))
    local startcol=1
    local lines=$( tput lines )
    local cols=$( tput cols )
    local colmax=$2
    local offset=$(( $cols / $colmax ))
    local size=$4
    shift 4
    trap "cursor_blink_on; stty echo; printf '\n'; exit" 2
    cursor_blink_off
    local active_row=0
    local active_col=0
    while true; do
        print_options_multicol $active_col $active_row
        case `key_input` in
            enter)  break;;
            up)     ((active_row--));
                    if [ $active_row -lt 0 ]; then active_row=0; fi;;
            down)   ((active_row++));
                    if [ $active_row -ge $(( ${#options[@]} / $colmax ))  ]; then active_row=$(( ${#options[@]} / $colmax )); fi;;
            left)     ((active_col=$active_col - 1));
                    if [ $active_col -lt 0 ]; then active_col=0; fi;;
            right)     ((active_col=$active_col + 1));
                    if [ $active_col -ge $colmax ]; then active_col=$(( $colmax - 1 )) ; fi;;
        esac
    done
    cursor_to $lastrow
    printf "\n"
    cursor_blink_on
    return $(( $active_col + $active_row * $colmax ))
}

filesystem () {
  echo -ne "
  ###################################################################
         Please Select your file system for both boot and root
  ###################################################################"
  options=("ext4" "luks" "exit")
  select_option $? 1 "${options[@]}"
  case $? in
  0) set_option FS ext4;;
  1)
  while true; do
    echo -ne "Please enter your luks password: \n"
    read -s luks_password
    echo -ne "Please repeat your luks password: \n"
    read -s luks_password2
    if [ "$luks_password" = "$luks_password2" ]; then
      set_option LUKS_PASSWORD $luks_password
      set_option FS luks
      break
    else
      echo -e "\nPasswords do not match. Please try again. \n"
    fi
  done
  ;;
  2) exit ;;
  *) echo "Wrong option! Please select again."; filesystem;;
  esac
}

timezone () {
  time_zone="$(curl --fail https://ipapi.co/timezone)"
  echo -ne "System detected your timezone to be '$time_zone' \n"
  echo -ne "Is this correct?"
  options=("Yes" "No")
  select_option $? 1 "${options[@]}"
  case ${options[$?]} in
      y|Y|yes|Yes|YES)
      echo "${time_zone} is set as timezone"
      set_option TIMEZONE $time_zone;;
      n|N|no|NO|No)
      echo "Please enter your desired timezone e.g. Europe/London :"
      read new_timezone
      echo "${new_timezone} is set as timezone"
      set_option TIMEZONE $new_timezone;;
      *) echo "Wrong option. Try again";timezone;;
  esac
}

keymap () {
  echo -ne "Please select keyboard layout from this list"
  options=(us by ca cf cz de dk es et fa fi fr gr hu il it lt lv mk nl no pl ro ru sg ua uk)
  select_option $? 4 "${options[@]}"
  keymap=${options[$?]}
  echo -ne "Your key boards layout: ${keymap} \n"
  set_option KEYMAP $keymap
}

drivessd () {
  echo -ne "Is this an ssd? yes/no: "
  options=("Yes" "No")
  select_option $? 1 "${options[@]}"
  case ${options[$?]} in
      y|Y|yes|Yes|YES)
      set_option MOUNT_OPTIONS "noatime,compress=zstd,ssd,commit=120";;
      n|N|no|NO|No)
      set_option MOUNT_OPTIONS "noatime,compress=zstd,commit=120";;
      *) echo "Wrong option. Try again";drivessd;;
  esac
}

disk () {
  echo -ne "Choose disk partition mode. Be AWARE that using custom partitions requires user to create partition before RUNNING THIS INSTALLATION SCRIPT!"
  options=("quickinstall" "custom")
  select_option $? 1 "${options[@]}"
  case ${options[$?]} in
      quickinstall)
        set_option CUSTOMPART no
        diskpart;;
      custom)
        echo -ne "Listing all available partitions using lsblk... "
        lsblk
        set_option CUSTOMPART yes
        echo -ne "Enter boot partition number (eg. '1' from /dev/sda1): "
        read CUSTOMDISKNUMBER1
        set_option CUSTOMDISKBOOT ${CUSTOMDISKNUMBER1}
        echo -ne "Enter root partition number (eg. '3' from /dev/sda3): "
        read CUSTOMDISKNUMBER2
        set_option CUSTOMDISKROOT ${CUSTOMDISKNUMBER2}
        diskpart;;
      *) echo "Wrong option. Try again";disk;;
      esac
}

diskpart () {
  echo -ne "
  ###################################################################
      THIS WILL FORMAT AND DELETE ALL DATA ON THE DISK
      Please make sure you know what you are doing because
      after formating your disk there is no way to get data back
  ###################################################################
  "
  PS3='
  Select the disk to install on: '
  options=($(lsblk -n --output TYPE,KNAME,SIZE | awk '$1=="disk"{print "/dev/"$2"|"$3}'))
  select_option $? 1 "${options[@]}"
  disk=${options[$?]%|*}
  echo -e "\n${disk%|*} selected \n"
      set_option DISK ${disk%|*}
  drivessd
}

userinfo () {
  read -p "Please enter your username: " username
  set_option USERNAME ${username,,}
  while true; do
    echo -ne "Please enter your password: \n"
    read -s password
    echo -ne "Please repeat your password: \n"
    read -s password2
    if [ "$password" = "$password2" ]; then
      set_option PASSWORD $password
      break
    else
      echo -e "\nPasswords do not match. Please try again. \n"
    fi
  done
  read -rep "Please enter your hostname: " nameofmachine
  set_option NAME_OF_MACHINE $nameofmachine
}

aurhelper () {
  echo -ne "Please enter your desired AUR helper:\n"
  options=(yay paru trizen none)
  select_option $? 4 "${options[@]}"
  aur_helper=${options[$?]}
  set_option AUR_HELPER $aur_helper
}

desktopenv () {
  echo -ne "Please select your desired Desktop Enviroment:\n"
  options=(dwm gnome openbox hyprland minimal)
  select_option $? 4 "${options[@]}"
  desktop_env=${options[$?]}
  set_option DESKTOP_ENV $desktop_env
}

installtype () {
  echo -ne "Please select type of installation:\n\n
  Full install: Installs full featured desktop enviroment, with added apps and themes needed for everyday use\n
  Minimal Install: Installs only apps few selected apps to get you started\n"
  options=(FULL MINIMAL)
  select_option $? 4 "${options[@]}"
  install_type=${options[$?]}
  set_option INSTALL_TYPE $install_type
}

bootloader () {
  echo -ne "Do you want to install a bootloader (recommended for UEFI systems)? yes/no: "
  options=("Yes" "No")
  select_option $? 1 "${options[@]}"
  case ${options[$?]} in
      y|Y|yes|Yes|YES)
      set_option INSTALL_BOOTLOADER "yes";;
      n|N|no|NO|No)
      set_option INSTALL_BOOTLOADER "no";;
      *) echo "Wrong option. Try again"; bootloader;;
  esac
}

clear
logo
userinfo
clear
logo
desktopenv
set_option INSTALL_TYPE MINIMAL
set_option AUR_HELPER NONE
if [[ ! $desktop_env == minimal ]]; then
  clear
  logo
  aurhelper
  clear
  logo
  installtype
fi
clear
logo
disk
clear
logo
filesystem
clear
logo
timezone
clear
logo
keymap
clear
logo
bootloader