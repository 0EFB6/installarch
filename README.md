## **Automated Arch Installation Script (Created by Willy)**

The script is meant to be used on Arch Linux only! It is also recommended to use it on UEFI system.

Before running the script, please make sure to install the required dependencies by using the command:
```bash
pacman -Sy git 
```

## **Run The Script:**
```bash
git clone https://gitlab.com/0efb6/installarch && cd installarch
./install.sh
```
All the code created was meant to ease Arch Installation. USE AT YOUR OWN RISK!